<?php
/**
 * Created by PhpStorm.
 * User: TianZineng
 * Date: 14-7-5
 * Time: 下午2:53
 */

namespace cache\drivers;


class Memcache {
    public $Memcache;
    /*
     * 构造函数
     * @param string $host 服务器
     * @param int $port 端口11211
     * @param bool $pconnect 是否持久化连接 默认否
     * @param int $timeout 超时时间 默认1s
     * */
    public function __construct($host,$port=11211,$pconnect=false,$timeout=1){
        $this->Memcache = new \Memcache();
        $pconnect?$this->Memcache->pconnect($host,$port,$timeout):$this->Memcache->connect($host,$port,$timeout);
    }
    /*
     * 设置值
     * @param string $key 键值
     * @param string $value 值
     * */
    public function set($key,$val,$timeout=0){
        return $this->Memcache->set($key,$val,$timeout);
    }
    /*
     * 取值
     * @param string $key 键值
     * */
    public function get($key){
        return $this->Memcache->get($key);
    }
    /*
     * 删除值
     * @param string $key 键值
     * */
    public function del($key){
        return $this->Memcache->delete($key);
    }
    /*
     * 刷新缓存(清除)
     * */
    public function clr(){
        return $this->Memcache->flush();
    }
    /*
     * 测试
     * */
    public function test(){
        echo 'This is a Redis!';
    }
} 