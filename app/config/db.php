<?php
return [
    'mysql' => [
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => '',
        'username'  => '',
        'password'  => '',
        'charset'   => 'utf8',
        'prefix'    => '',
        'port'      => '3306'
    ],
    'cache' => [
        'driver'    => 'redis',
        'host'      => 'localhost',
        'port'      => '6379'
    ]
];
