<?php
/**
 * Created by PhpStorm.
 * User: TianZineng
 * Date: 14-7-3
 * Time: 下午9:35
 */

#namespace Remix;

/**
 * 缓存管理类
 * 调用时可以直接Cache::set($key,$value)
 */
class Cache {
    public static $cache;
    public function __construct(){
        $cacheConf = C('cache');
        $drivers = $cacheConf['drivers'];
        $host = $cacheConf['host'];
        $port = $cacheConf['port'];
        $drivers = ucwords(strtolower($drivers));
        $filePath = 'cache/drivers/'.$drivers.'.php';
        if(file_exists($filePath)){
            include $filePath;
            static::$cache = new $drivers();
        }
    }
    public static function set($key,$value){
        return static::$cache->set($key,$value);
    }
    public static function get($key){
        return static::$cache->get($key);
    }
    public static function del($key){
        return static::$cache->del($key);
    }
    public static function clr(){
        return static::$cache->clr();
    }
}