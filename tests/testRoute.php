<?php
    define('REMIX_CORE', __DIR__ . '/../Remix');
    define('DS', DIRECTORY_SEPARATOR);
    define('EXT', '.php');

    //自动加载
    require_once REMIX_CORE . DS . 'autoloader.php';
    require_once REMIX_CORE . DS . 'helpers.php';

    spl_autoload_register(array('Remix\\Autoloader', 'load'));
    Remix\Autoloader::namespaces(array('Remix' => REMIX_CORE));

    $testUri = 'about';
    $testReg = '(:num)/(:any)/(:num?)/(:any?)';

    //路由注册
    Remix\Route::get($testUri, function(){
        return 'hello world';
    });

    Remix\Route::get($testReg, function(){
        return 'mixed';
    });


    //调度者解析路由
    $route = Remix\Dispatcher::parse('GET', $testUri);
    //断言
    assert_eq($route->call(), 'hello world');