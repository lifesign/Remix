<?php 
    use Remix\Route;

    Route::get('/', function(){
        return 'hello remix phpframework';
    });

    Route::get('/hello/(:any)', function($name){
        return "hello {$name}";
    });

    Route::post('/', function(){
        return 'post action';
    });

