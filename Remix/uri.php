<?php namespace Remix;

class URI
{
    public static function current() {
        //格式化path 如果是根路径 返回/
        return static::format(Request::getPathInfo());
    }

    /**
     * 格式化给定的uri 去除两边的/ 如果是根路径返回/
     *
     * @param  string  $uri
     * @return string
     */
    protected static function format($uri)
    {
        return trim($uri, '/') ?: '/';
    }
}