<?php

/**
 * Remix必须运行在5.3版本以上
 */
if(version_compare(PHP_VERSION, '5.3') < 0) {
    echo 'We need PHP 5.3 or higher, you are running ' . PHP_VERSION;
    exit;
}

/**
 * 去除魔术引号
 */
if (get_magic_quotes_gpc()) {
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);

    array_walk_recursive($gpc, function(&$value) {
        $value = stripslashes($value);
    });
}

#开启ob
ob_start('mb_output_handler');

/**
 * 加载框架核心文件 这里显示的加载用于提高效率
 */
require REMIX_ROOT . 'helpers'.EXT;
// require REMIX_ROOT . 'hook'.EXT; //todo
require REMIX_ROOT . 'error'.EXT;
require REMIX_ROOT . 'config'.EXT;
require REMIX_ROOT . 'autoloader'.EXT;

#注册自动加载
#1. 如果采用composer 默认用composer
if (file_exists($composer = '../vendor/autoload.php')) require $composer;
#2. 注册框架自带的自动加载机制
spl_autoload_register(array('Remix\\Autoloader', 'load'));

#注册Remix命名空间
Remix\Autoloader::namespaces(array('Remix' => REMIX_ROOT));
#注册aliase 这样就不用每次敲过多的命名空间
Remix\Autoloader::$aliases = (array)Remix\Config::get('app.aliases');

#处理error_hanler
set_exception_handler(array('Remix\\Error', 'exception'));
set_error_handler(array('Remix\\Error', 'native'));
register_shutdown_function(array('Remix\\Error', 'shutdown'));