<?php namespace Remix;

class View
{
    public $data;
    public $temp_path;
    public $return_val;

    function __construct($data,$temp_path){
        $this->data = $data;
        $this->temp_path = $temp_path;
    }

    //返回一个自己的实例
    public static function make($data,$temp_path){
        return new self($data,$temp_path);
    }

    //设置变量名和值，链式调用
    public function assign($name,$value){
        $this->data[$name] = $value;
        return $this;
    }

    public function __toString(){
        return $this->display();
    }

    //返回最终内容字符串
    public function display() {
        ob_start();
        extract($this->data);
        include $this->temp_path;
        //include APP_ROOT.$temp_path;
        $this->return_val = ob_get_contents();
        ob_end_clean();

        return $this->return_val;
    }
}