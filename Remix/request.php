<?php namespace Remix;

/**
 * http 请求类 封装了 请求的一些信息
 */
class Request
{
    //symfony http foundation
    public static $foundation;

    public static function ip($default = '0.0.0.0')
    {
        $client_ip = static::foundation()->getClientIp();
        return $client_ip === NULL ? $default : $client_ip;
    }

    public static function method() {
        $method = static::foundation()->getMethod();

        return ($method == 'HEAD') ? 'GET' : $method;
    }

    public static function ajax()
    {
        return static::foundation()->isXmlHttpRequest();
    }

    public static function foundation()
    {
        return static::$foundation;
    }

    public static function __callStatic($method, $parameters)
    {
        return call_user_func_array(array(static::foundation(), $method), $parameters);
    }

}
