<?php
/*
 * 主配置文件
 * 其它自定义可以分文件配置，需要在本文件合并。
 */
$config=[
    'DEFAULT_TIMEZONE'      =>  'PRC',	// 默认时区

];
return array_merge(
    require(__DIR__.'/db.php'),
    require(__DIR__.'/app.php'),
    $config
);
