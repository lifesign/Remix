<?php namespace Remix;
    #框架加载文件
    define('EXT', '.php');

    require REMIX_ROOT . 'core' . EXT;
    require REMIX_ROOT . 'hook' . EXT;
    $convention=require REMIX_ROOT . 'convention' . EXT;
//    Hook::add('run','Remix\\test');
    Hook::import($convention['tags']);
    Hook::listen('run');

    #使用symfony作为request的foundation
    use Symfony\Component\HttpFoundation\Request as RequestFoundation;

    RequestFoundation::enableHttpMethodParameterOverride();
    Request::$foundation = RequestFoundation::createFromGlobals();

    //首先注册一个通用路由 响应为404
    //只有对应了注册的路由才会执行到响应的请求
    Dispatcher::register('*', '(:all)', function()
    {
        return '404 error';
    });

    //加载应用路由
    file_exists($app_routes = APP_ROOT . 'routes' . EXT) && require $app_routes;
    //todo hook
    //Hook::fire('before_route');
    $route = Dispatcher::parse(Request::method(), URI::current());

    $response = $route->call();
    //Hook::fire('after_route');
    //Hook::fire('before_response_render');
    $response->render();
    //Hook::fire('after_response_render');

    #发送响应到浏览器
    $response->send();

    //Hook::fire('cycle_over', array($response));