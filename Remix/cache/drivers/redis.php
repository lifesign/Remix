<?php
/**
 * Created by PhpStorm.
 * User: TianZineng
 * Date: 14-7-5
 * Time: 下午2:54
 */

namespace cache\drivers;

class Redis{
    public $Redis;
    /*
     * 构造函数
     * @param string $host 服务器
     * @param int $port 端口6379
     * @param bool $pconnect 是否持久连接 默认false
     * @param int $timeout 连接时长 默认0永久
     * */
    public function __construct($host,$port=6379,$pconnect=false,$timeout=0){
        $this->Redis = new \Redis();
        $pconnect?$this->Redis->pconnect($host,$port,$timeout):$this->Redis->connect($host,$port,$timeout);
    }
    /*
     * 设置值
     * @param string $key 键值
     * @param string $value 值
     * */
    public function set($key,$val,$timeout=0){
        return $this->Redis->set($key,$val,$timeout);
    }
    /*
     * 取值
     * @param string $key 键值
     * */
    public function get($key){
        return $this->Redis->get($key);
    }
    /*
     * 删除值
     * @param string $key 键值
     * */
    public function del($key){
        return $this->Redis->delete($key);
    }
    /*
     * 刷新缓存(清除)
     * */
    public function clr(){
        return $this->Redis->flushDB();
    }

    /*
     * 测试
     * */
    public function test(){
        echo 'This is a Redis!';
    }
}